python-unicodecsv (0.14.1-7) unstable; urgency=medium

  * Fix debian/watch

 -- Martin <debacle@debian.org>  Mon, 27 Feb 2023 21:59:04 +0000

python-unicodecsv (0.14.1-6) unstable; urgency=medium

  * Fix test for Python 3.11 (Closes: #1026602)
    Thanks to s3v <c0llapsed@yahoo.it> for the right hint!

 -- Martin <debacle@debian.org>  Sat, 21 Jan 2023 23:38:42 +0000

python-unicodecsv (0.14.1-5) unstable; urgency=medium

  * Fix dependency for autopkgtest

 -- Martin <debacle@debian.org>  Wed, 02 Nov 2022 01:18:17 +0000

python-unicodecsv (0.14.1-4) unstable; urgency=medium

  * Add autopkgtest
  * Add Rules-Requires-Root=no

 -- Martin <debacle@debian.org>  Mon, 17 Oct 2022 22:42:31 +0000

python-unicodecsv (0.14.1-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from deprecated 7 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Sandro Tosi <morph@debian.org>  Sat, 04 Jun 2022 15:45:00 -0400

python-unicodecsv (0.14.1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * Convert git repository from git-dpm to gbp layout

  [ Thomas Bechtold ]
  * Remove myself from Uploaders; Closes: #892681

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

  [ Sandro Tosi ]
  * Drop python2 support; Closes: #938239

 -- Sandro Tosi <morph@debian.org>  Sun, 15 Dec 2019 15:18:24 -0500

python-unicodecsv (0.14.1-1) unstable; urgency=medium

  * New upstream release

 -- W. Martin Borgert <debacle@debian.org>  Mon, 30 Nov 2015 21:28:21 +0000

python-unicodecsv (0.13.0-2) unstable; urgency=low

  * Add python3-setuptools build-dep (Closes: #795585), thanks Chris Lamb.

 -- W. Martin Borgert <debacle@debian.org>  Sat, 15 Aug 2015 19:34:33 +0000

python-unicodecsv (0.13.0-1) unstable; urgency=low

  * New upstream release
  * Moved repo to git
  * New watch file, thanks to bartm
  * Add Python 3 package
  * Bump standards version, no changes
  * Fix license short name

 -- W. Martin Borgert <debacle@debian.org>  Mon, 03 Aug 2015 21:34:56 +0000

python-unicodecsv (0.9.4-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Thomas Bechtold ]
  * New upstream release.
  * debian/patches/01fix_array_float_repr.diff: Removed, applied upstream.
  * debian/python-unicodecsv.docs: Replace README with README.rst.
  * debian/control:
    - Bump Standards-Version to 3.9.4.
    - Add me to Uploaders.
    - Build-Depends on python-unittest2 (>= 0.5.1) according to setup.py.

 -- Thomas Bechtold <thomasbechtold@jpberlin.de>  Tue, 21 May 2013 16:15:25 +0200

python-unicodecsv (0.9.0-1) unstable; urgency=low

  [ Thomas Bechtold <thomasbechtold@jpberlin.de>]:
  * Initial Release (Closes: #669678)

 -- W. Martin Borgert <debacle@debian.org>  Mon, 14 May 2012 18:41:11 +0000
